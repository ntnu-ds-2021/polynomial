//
// Created by jw910731 on 2021/10/22.
//

#ifndef POLYNOMIAL_POLYNOMIAL_H
#define POLYNOMIAL_POLYNOMIAL_H

#include <string>
#include <algorithm>

struct term {
    double coeff;
    int exp;
    term() = default;
    term(double _coeff, double _exp) : coeff(_coeff), exp(_exp) {}
    bool operator<(const term &t) const {
        if (this->exp == t.exp)
            return this->coeff < t.coeff;
        return this->exp < t.exp;
    }
    bool operator>(const term &t) const {
        if (this->exp == t.exp)
            return this->coeff > t.coeff;
        return this->exp > t.exp;
    }
};

std::ostream &operator<<(std::ostream &os, const term &t){
    os << t.coeff;
    if(t.exp > 0) os << 'x';
    if(t.exp >1) os << '^' << t.exp;
    return os;
}

// Container is specifically refers to SequenceContainer (std::array doesn't fit)
template <typename Container = std::vector<term>>
class polynomial {
public:
    polynomial() = default;
    // sort by exp incrementally
    Container terms;
    explicit polynomial(Container _terms) : terms(_terms) {  }

    // leave non zero terms
    void clean(){
        Container tmp;
        for(auto it = terms.cbegin() ; it != terms.cend() ; ++it){
            if(it->coeff != 0) tmp.emplace_back(*it);
        }
        terms = tmp;
    }

    // polynomial addition
    template <class C>
    polynomial<Container> operator+(const polynomial<C> &p) const noexcept {
        polynomial ret = *this;
        return ret += p;
    }

    template <class C>
    polynomial<Container> &operator+=(const polynomial<C> &p) noexcept {
        clean();
        Container tmp;
        auto t_it = this->terms.cbegin();
        auto p_it = p.terms.cbegin();
        // overall time complexity will be O(M+N), where M, N is the term number of the polynomials
        for (; t_it != this->terms.cend() && p_it != p.terms.cend() ;) {
            // same exp = combine (when available)
            if (t_it->exp == p_it->exp) {
                tmp.emplace_back(t_it->coeff + p_it->coeff, t_it->exp);
                ++t_it;
                ++p_it;
            } else if (t_it->exp > p_it->exp) {
                tmp.emplace_back(*(p_it++)); // emplace smaller term into tmp
            } else {
                tmp.emplace_back(*(t_it++));
            }
        }
        // process remaining terms (these two loops are logical exclusive)
        for (; p_it != p.terms.cend() ; ++p_it)
            tmp.emplace_back(*p_it);
        for(; t_it != this->terms.cend() ; ++t_it){
            tmp.emplace_back(*t_it);
        }
        this->terms = tmp;
        return *this;
    }

    // polynomial subtraction
    template <class C>
    polynomial<Container> operator-(const polynomial<C> &p) const noexcept {
        polynomial ret = *this;
        return ret -= p;
    }
    template <class C>
    polynomial<Container> &operator-=(const polynomial<C> &p) noexcept{
        clean();
        Container tmp;
        auto t_it = this->terms.cbegin();
        auto p_it = p.terms.cbegin();
        // overall time complexity will be O(M+N), where M, N is the term number of the polynomials
        for (; t_it != this->terms.cend() && p_it != p.terms.cend();) {
            // same exp = combine (when available)
            if (t_it->exp == p_it->exp) {
                tmp.emplace_back(t_it->coeff - p_it->coeff, t_it->exp);
                ++t_it;
                ++p_it;
            } else if (t_it->exp > p_it->exp) {
                tmp.emplace_back(p_it->coeff * -1, p_it->exp);
                ++p_it;
            } else {
                tmp.emplace_back(*(t_it++));
            }
        }
        // process remaining terms (these two loops are logical exclusive)
        for (; p_it != p.terms.cend(); ++p_it){
            tmp.emplace_back(p_it->coeff * -1, p_it->exp);
        }
        for(;t_it != this->terms.cend(); ++t_it){
            tmp.emplace_back(*t_it);
        }
        this->terms = tmp;
        return *this;
    }

    // polynomial term query
    term &operator[](int exp) noexcept {
        // takes linear complexity
        for (auto it = this->terms.begin() ; it != this->terms.end() ; ++it){
            if(it->exp == exp) return *it;
        }

        // when term not found, create one
        for(auto it = this->terms.begin() ; it != this->terms.end() ; ++it){
            if(it->exp <= exp){
                return *(this->terms.emplace(it, 0, exp));
            }
        }
        return *(this->terms.emplace(this->terms.end(), 0, exp));
    }

    void remove(int exp){
        for (auto it = this->terms.begin() ; it != this->terms.end() ; ++it){
            if(it->exp == exp){
                it->coeff = 0;
                return;
            }
        }
        throw std::logic_error("term not found");
    }

    // polynomial multiplication
    template <class C>
    polynomial<Container> operator*(const polynomial<C> &p) const noexcept {
        polynomial ret = *this;
        return ret *= p;
    }
    template <class C>
    polynomial<Container> operator*=(const polynomial<C> &p){
        clean();
        polynomial<Container> accumulator;
        // double loop takes time complexity O(N*M)
        for(auto t_it = this->terms.cbegin() ; t_it != this->terms.cend() ; ++t_it){
            polynomial<Container> sub_acc;
            for(auto p_it = p.terms.cbegin() ; p_it != p.terms.cend() ; ++p_it){
                sub_acc.terms.emplace_back(term(t_it->coeff * p_it->coeff, t_it->exp + p_it->exp));
            }
            accumulator += sub_acc;
        }
        // takes linear time complexity to copy
        this->terms = accumulator.terms;
        return *this;
    }

    template <class C>
    friend std::ostream &operator<<(std::ostream &os, const polynomial<C> &p) noexcept;
};

template <class C>
std::ostream &operator<<(std::ostream &os, const polynomial<C> &p) noexcept {
    bool lp = false;
    for (auto it = p.terms.crbegin(); it != p.terms.crend(); ++it) {
        if(it->coeff != 0) {
            auto &t = *it;
            if (lp)
                os << "-+"[t.coeff > 0] << ' ';
            if (abs(t.coeff) != 1 || t.exp == 0)
                os << ((lp) ? abs(t.coeff) : t.coeff);
            else if(t.coeff == -1 && !lp){
                os << '-';
            }
            os << ((t.exp != 0) ? "x" : "");
            if (t.exp > 1) {
                os << "^" << t.exp;
            }
            os << ' ';
            lp = true;
        }
    }
    return os;
}
#endif // POLYNOMIAL_POLYNOMIAL_H
