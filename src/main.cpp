//
// Created by jw910731 on 2021/10/22.
//
#include <iostream>
#include <list>
#include <vector>
#include <sstream>
#include <map>

#include "polynomial.h"

template<typename C = std::vector<term>>
polynomial<C> parser(const std::string &s);

// std::list specialization
template<>
polynomial<std::list<term>> parser<std::list<term>>(const std::string &s);

int main() {
    bool run = true;
    std::map<std::string, polynomial<std::vector<term>>> poly_map;
    for( ; run ; ){
        char cmd;
        std::cout << "1) Input polynomial\n"
                     "2) Display polynomial\n"
                     "3) Query polynomial term coefficient\n"
                     "4) Add term to polynomial\n"
                     "5) Remove term from polynomial\n"
                     "6) Add polynomials\n"
                     "7) Subtract polynomials\n"
                     "8) Multiply polynomials\n"
                     "0) Quit\n";
        std::cout << "Input one of the option: ";
        std::cin >> cmd;
        getchar(); // remove remaining newline in stdin
        switch(cmd){
            case '0':
                run = false;
                break;
            case '1': {
                std::cout << "Input polynomial name: ";
                std::string name;
                getline(std::cin, name);
                std::cout << "Input polynomial (in ax ^ b + cx ^ d form): ";
                std::string input;
                getline(std::cin, input);
                try{
                    auto tmp = parser<std::vector<term>>(input);
                    poly_map[name] = tmp;
                }
                catch (std::logic_error &e) {
                    std::cout << "Error:" << e.what() << '\n';
                }
                break;
            }
            case '2': {
                std::cout << "Input polynomial name: ";
                std::string name;
                getline(std::cin, name);
                auto val = poly_map.find(name);
                if(val != poly_map.end())
                    std::cout << "Result: " << val->second << '\n';
                else
                    std::cout << "Error: cannot find polynomial with name : " << name << '\n';
                break;
            }
            case '3': {
                std::cout << "Input polynomial name: ";
                std::string name;
                getline(std::cin, name);
                std::cout << "Input exponential of the query term: ";
                int exp;
                std::cin >> exp;
                getchar(); // remove remaining newline in stdin
                auto val = poly_map.find(name);
                if(val != poly_map.end())
                    std::cout << val->second[exp];
                else
                    std::cout << "Error: cannot find polynomial with name : " << name << '\n';
                break;
            }
            case '4': {
                std::cout << "Input polynomial name: ";
                std::string name;
                getline(std::cin, name);
                std::cout << "Input coefficient of adding term: ";
                double coeff;
                std::cin >> coeff;
                std::cout << "Input exponential of adding term: ";
                int exp;
                std::cin >> exp;
                getchar(); // remove remaining newline in stdin
                auto val = poly_map.find(name);
                if(val != poly_map.end()) {
                    val->second[exp] = term(coeff, exp);
                }
                else
                    std::cout << "Error: cannot find polynomial with name : " << name << '\n';
                break;
            }
            case '5':
            {
                std::cout << "Input polynomial name: ";
                std::string name;
                getline(std::cin, name);
                std::cout << "Input exponential of removing term: ";
                int exp;
                std::cin >> exp;
                getchar(); // remove remaining newline in stdin
                auto val = poly_map.find(name);
                if(val != poly_map.end()) {
                    try{
                        val->second.remove(exp);
                    }
                    catch (std::logic_error &e){
                        std::cout << "Error: " << e.what() << '\n';
                    }

                }
                else
                    std::cout << "Error: cannot find polynomial with name : " << name << '\n';
                break;
            }
            case '6': {
                std::string name1, name2;
                std::cout << "Input adder's name (use enter to separate): ";
                getline(std::cin, name1);
                getline(std::cin, name2);
                auto it1 = poly_map.find(name1), it2 = poly_map.find(name2);
                if(it1 == poly_map.end() || it2 == poly_map.end()){
                    std::cout << "Error: cannot find polynomial" << '\n';
                    break;
                }
                std::cout << "Result: " << it1->second + it2->second << '\n';
                break;
            }
            case '7':{
                std::string name1, name2;
                std::cout << "Input subtractors' name (use enter to separate, first one minus second one): ";
                getline(std::cin, name1);
                getline(std::cin, name2);
                auto it1 = poly_map.find(name1), it2 = poly_map.find(name2);
                if(it1 == poly_map.end() || it2 == poly_map.end()){
                    std::cout << "Error: cannot find polynomial" << '\n';
                    break;
                }
                std::cout << "Result: " << it1->second - it2->second << '\n';
                break;
            }
            case '8':{
                std::string name1, name2;
                std::cout << "Input multiplier' name (use enter to separate): ";
                getline(std::cin, name1);
                getline(std::cin, name2);
                auto it1 = poly_map.find(name1), it2 = poly_map.find(name2);
                if(it1 == poly_map.end() || it2 == poly_map.end()){
                    std::cout << "Error: cannot find polynomial" << '\n';
                    break;
                }
                std::cout << "Result: " << it1->second * it2->second << '\n';
                break;
            }
            default:
                std::cout << "<Invalid option!>\n";
                break;
        }
    }
    return 0;
}

template<typename C>
polynomial<C> parser(const std::string &s){
    C tmp;

    const char *str = s.c_str();

    for(const char *it = str ; *it ; ){
        int sign = 1;
        for(; std::isspace(*it) && *it ; ++it); // eat all space
        if(!*it) break; // break if it's the end of string

        // process sign
        if(*it == '-'){
            sign = -1;
            ++it;
        }else if(*it == '+'){
            ++it;
        }

        for(; std::isspace(*it) && *it ; ++it); // eat all space
        if(!*it) break; // break if it's the end of string

        // process coefficient
        const char *ed_it = it;
        for(; (std::isdigit(*ed_it) || *ed_it == '.') && *ed_it ; ++ed_it); // find number end
        std::stringstream ss;
        ss.write(it, ed_it - it);
        double coeff = 1.0;
        ss >> coeff;
        coeff *= sign;
        for(; std::isspace(*it) && *it ; ++it); // eat all space
        if(!*it) break;
        it = ed_it;

        for(; std::isspace(*it) && *it ; ++it); // eat all space
        if(!*it){
            tmp.emplace_back(coeff, 0);
            break;
        }

        // process exponential
        if(*it == 'x'){ // it meet the "x"
            it++;
            int exp;
            for(; std::isspace(*it) && *it ; ++it); // eat all space
            if(*it == '^'){ // exponential not 1
                it++;
                for(; std::isspace(*it) && *it ; ++it); // eat all space
                if(!*it) throw std::logic_error("invalid input string");

                ed_it = it;
                for(; std::isdigit(*ed_it) && *ed_it ; ++ed_it);
                ss.clear();
                ss.write(it, ed_it - it);
                ss >> exp;
                it = ed_it; // increment
            }
            else{
                exp = 1;
            }
            tmp.emplace_back(coeff, exp);
        }
        else{
            for(; std::isspace(*it) && *it ; ++it); // eat all space
            if(!*it){ // there might exist remaining constant term
                tmp.emplace_back(coeff, 0);
                break;
            }
            if(*it == '+' || *it == '-' || std::isspace(*it)) {
                tmp.emplace_back(coeff, 0);
                continue;
            }
            throw std::logic_error("invalid input string");
        }
    }
    std::sort(tmp.begin(), tmp.end());
    // merge
    C ret;
    auto t_it = ret.begin();
    for(const auto &it : tmp){
        if(t_it != ret.end() && t_it->exp < it.exp){
            t_it = ret.emplace(std::next(t_it), 0, it.exp);
        }
        if(t_it == ret.end()){
            t_it = ret.emplace(t_it, 0, it.exp);
        }
        t_it->coeff += it.coeff;
    }
    return polynomial(ret);
}

// std::list specialization
template <>
polynomial<std::list<term>> parser<std::list<term>>(const std::string &s){
    std::list<term> tmp;

    const char *str = s.c_str();

    for(const char *it = str ; *it ; ){
        int sign = 1;
        for(; std::isspace(*it) && *it ; ++it); // eat all space
        if(!*it) break; // break if it's the end of string

        // process sign
        if(*it == '-'){
            sign = -1;
            ++it;
        }else if(*it == '+'){
            ++it;
        }

        for(; std::isspace(*it) && *it ; ++it); // eat all space
        if(!*it) break; // break if it's the end of string

        // process coefficient
        const char *ed_it = it;
        for(; (std::isdigit(*ed_it) || *ed_it == '.') && *ed_it ; ++ed_it); // find number end
        std::stringstream ss;
        ss.write(it, ed_it - it);
        double coeff = 1.0;
        ss >> coeff;
        coeff *= sign;
        for(; std::isspace(*it) && *it ; ++it); // eat all space
        if(!*it) break;
        it = ed_it;

        for(; std::isspace(*it) && *it ; ++it); // eat all space
        if(!*it){
            tmp.emplace_back(coeff, 0);
            break;
        }

        // process exponential
        if(*it == 'x'){ // it meet the "x"
            it++;
            int exp;
            for(; std::isspace(*it) && *it ; ++it); // eat all space
            if(*it == '^'){ // exponential not 1
                it++;
                for(; std::isspace(*it) && *it ; ++it); // eat all space
                if(!*it) throw std::logic_error("invalid input string");

                ed_it = it;
                for(; std::isdigit(*ed_it) && *ed_it ; ++ed_it);
                ss.clear();
                ss.write(it, ed_it - it);
                ss >> exp;
                it = ed_it; // increment
            }
            else{
                exp = 1;
            }
            tmp.emplace_back(coeff, exp);
        }
        else{
            for(; std::isspace(*it) && *it ; ++it); // eat all space
            if(!*it){ // there might exist remaining constant term
                tmp.emplace_back(coeff, 0);
                break;
            }
            if(*it == '+' || *it == '-' || std::isspace(*it)) {
                tmp.emplace_back(coeff, 0);
                continue;
            }
            throw std::logic_error("invalid input string");
        }
    }
    tmp.sort();
    // merge
    std::list<term> ret;
    auto t_it = ret.begin();
    for(const auto &it : tmp){
        if(t_it == ret.begin()){
            t_it = ret.emplace(t_it, 0, it.exp);
        }
        else if(t_it->exp < it.exp){
            t_it = ret.emplace(std::next(t_it), 0, it.exp);
        }
        t_it->coeff += it.coeff;
    }
    return polynomial(ret);
}