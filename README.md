# Polynomial

Assignment 1 : Implement a polynomial operation system.

# License
A modified version of [DWYW](https://github.com/thornjad/DWYW) license.

The modification is for prohibiting usage in NTNU Data Structure course.